import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { categorys, Category, Product } from '../statement'
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  @Input() categorys: any;
  @Input() selectProducts: any;
	@Input() selectCategory: any;
  @Input() selectProduct: any;

  @Output() newCategoryItemEvent = new EventEmitter<Category>();
  @Output() newProductItemEvent = new EventEmitter<Product>();
  @Output() newQuery = new EventEmitter();
  constructor() {}
  ngOnInit(): void {}
  onChildSelectCategory(category: Category): void {
    this.newCategoryItemEvent.emit(category)

  }
  onChildSelectProduct(product: Product): void {
    this.newProductItemEvent.emit(product)
  }
  onQuery(): void {
    this.newQuery.emit()
  }
  
}
