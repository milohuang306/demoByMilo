import { Component, OnInit, ViewChild } from '@angular/core';
import { categorys, Category, Product, digitalProducts, pantsProducts, nursingProducts, ListItem } from '../statement'
import { ListService } from '../list.service'
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
	categorys = categorys
	selectCategory:Category = categorys[0]
	selectCategoryTypeName: String = categorys[0].typeName
	selectProducts: Product[] = [] 
	selectProduct: Product = {
		type: this.selectCategory,
		itemType: '',
		itemTypeName: ''
	}
  datas: ListItem[] = []
  updateTimer: any = null
  updateTimerByQuery: any = null
  constructor(private listService: ListService) {}
  ngOnInit(): void {
  	this.updateSelectProducts()
    this.onQuery()
    this.intervalQuery()
  }
  updateSelectProducts():void {
  	if (this.selectCategory.type == 'all') {
  		this.selectProduct = {
				type: this.selectCategory,
				itemType: '',
				itemTypeName: ''
			}
			return
  	}
  	[digitalProducts, pantsProducts, nursingProducts].forEach((items, idx) => {
  		if (items[0] && items[0].type.type === this.selectCategory.type) {
  			this.selectProducts = items
  			this.selectProduct = items[0]
  		}
  	})
  }
  onSelectCategory(category: any):void {
  	if (category.type !== this.selectCategory.type) {
  		this.selectCategory = category
  		this.updateSelectProducts()
  		this.onQuery()
  	}
  }
  onSelectProduct(product: Product) {
  	if (this.selectProduct.itemTypeName !== product.itemTypeName) {
  		this.selectProduct = product
  		this.onQuery()
  	}
  }
  onQuery():void {
    clearInterval(this.updateTimer)
    let list:ListItem[] = this.listService.query({
        type: this.selectCategory.type,
        itemType: this.selectProduct.itemType})
    this.datas = [];
    if (list.length > 0) {
      this.updateTimer = setInterval((): void =>{
        if (list[0]) {
          this.datas.push(list[0])
          list.shift()
        } else {
          clearInterval(this.updateTimer)
        }
      }, 10)
    }
    
  }
  ngOnDestory(): void {
    clearInterval(this.updateTimerByQuery)
    clearInterval(this.updateTimer)
  }
  intervalQuery(): void {
    this.updateTimerByQuery = setInterval(():void => {
      this.onQuery()
    }, 5 * 1000)
  }
}
