export interface Category {
	type: String;
	typeName: String;
}

export interface Product {
	type: Category;
	itemType: String;
	itemTypeName: String;
}

export interface ListItem {
  type: string
  typeName: string;
  itemType: string
  itemTypeName: string;
  price: string;
  id: number,
  name: string;
  imgurl: string;
  desc: string;
  shopName: string;
  address: string;
}
export const categorys: Category[] = [
	{
		type: 'all',
		typeName: '全部'
	},
	{
		type: 'digital',
		typeName: '数码'
	},
	{
		type: 'clothing',
		typeName: '衣服'
	},
	{
		type: 'nursing',
		typeName: '护理'
	}
]

export const digitalProducts: Product[] = [
	{
		type: categorys[1],
		itemType: 'laptop',
		itemTypeName: '电脑'
	},
	{
		type: categorys[1],
		itemType: 'phone',
		itemTypeName: '手机'
	}
]

export const pantsProducts: Product[] = [
	{
		type: categorys[2],
		itemType: 'jacket',
		itemTypeName: '上衣'
	},
	{
		type: categorys[2],
		itemType: 'pants',
		itemTypeName: '裤子'
	}
]
export const nursingProducts: Product[] = [
	{
		type: categorys[3],
		itemType: 'cleasingMilk',
		itemTypeName: '洗面奶'
	},
	{
		type: categorys[3],
		itemType: 'facialMask',
		itemTypeName: '面膜'
	}
]