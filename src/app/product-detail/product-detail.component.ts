import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ListItem } from '../statement'
import { ListService } from '../list.service'
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
	item:any;
  constructor(private listService: ListService,
    private route: ActivatedRoute,
    private location: Location,
    private elRef: ElementRef) { }

  ngOnInit(): void {
  	this.queryItem()
    let img1Box = this.elRef.nativeElement.querySelector('.img-box')
    let mask = this.elRef.nativeElement.querySelector('.mask')
    let img2 = this.elRef.nativeElement.querySelector('.img2')
    let img2Box = this.elRef.nativeElement.querySelector('.img2-box')
    // 缩略图盒子的宽高
    const img1BoxWidth = 300
    const img1BoxHeight = 300
    // 阴影盒子的宽高
    const maskBoxWidth = 100
    const maskBoxHeight = 100
    img1Box.addEventListener('mousemove', (e:any): void => {
      mask.style.display = 'block';
      img2Box.style.display = 'block';
      // 缩略图 到 窗口 上 左的距离
      const img1BoxOffsetTop = img1Box.getBoundingClientRect().top
      const img1BoxoffsetLeft = img1Box.getBoundingClientRect().left
      const clientY = e.clientY
      const clientX = e.clientX
      // 计算鼠标到图片盒子模型的左上距离
      const top = clientY - img1BoxOffsetTop
      const left = clientX - img1BoxoffsetLeft
      // 计算阴影盒子移动的位置
      if (top < maskBoxHeight/2) mask.style.top = '0px'
      if (top > maskBoxHeight/2 && top > img1BoxHeight - maskBoxHeight/2) mask.style.top = img1BoxHeight - maskBoxHeight + 'px'
      if (top > maskBoxHeight/2 && top < img1BoxHeight - maskBoxHeight/2) mask.style.top = top - maskBoxHeight/2 + 'px'
      if (left < maskBoxWidth/2) mask.style.left = '0px'
      if (left > maskBoxWidth/2 && left > img1BoxWidth - maskBoxWidth/2) mask.style.left = img1BoxWidth - maskBoxWidth + 'px'
      if (left > maskBoxWidth/2 && left < img1BoxWidth - maskBoxWidth/2) mask.style.left = left- maskBoxWidth/2 + 'px'
      // 以上是阴影框跟随鼠标移动的逻辑
      // 放大镜功能 移动图片
      const maskPosX = parseInt(mask.style.left)
      const maskPosY = parseInt(mask.style.top)
      // 放大图展示区域是阴影部分的3倍
      const img2PosX = maskPosX * 3
      const img2PosY = maskPosY * 3
      // 放大图的移动是反向的 所以要加负号
      img2.style.left = -img2PosX + 'px'
      img2.style.top = -img2PosY + 'px'
    })
    img1Box.addEventListener('mouseout', (e:any): void => {
      mask.style.display = 'none';
      img2Box.style.display = 'none';
    })
  }
  queryItem(): void {
  	const id = Number(this.route.snapshot.paramMap.get('id'));
  	const item: ListItem = this.listService.queryById(id)
  	this.item = item
  }
  goback(): void {
  	this.location.back()
  }
}
