import { Injectable } from '@angular/core';
import { ListItem } from './statement'
import { phone } from '../testdata/phone'
import { pants } from '../testdata/pants'
import { laptop } from '../testdata/laptop'
import { jacket } from '../testdata/jacket'
import { facialMask } from '../testdata/facialMask'
import { cleasingMilk } from '../testdata/cleasingMilk'

let dataArr: ListItem[] = phone.concat(pants, laptop, jacket, facialMask, cleasingMilk)
// 生成ID
dataArr.forEach((item, idx) => {
  item.id = idx + 1
})
let randomList = (): void => {
	// 打乱
	for (let i = 0;i < 500; i++) {
		dataArr.sort(():number => {
			return Math.random() - 0.5
		})
	}
}
// randomList()
@Injectable({
  providedIn: 'root'
})
export class ListService {
  constructor() { }
  query(obj: any): ListItem[]{
  	obj = obj || {}
  	let list: ListItem[] = []
  	randomList()
  	let count = 0
  	for (var i = 0; i < dataArr.length; i ++) {
  		let item = dataArr[i]
  		if (obj.type === 'all') {
  			count += 1
  			list.push(item)
  		} else {
  			if (obj.type === item.type && obj.itemType === item.itemType) {
  				count += 1
  				list.push(item)
  			}
  		}
  		if (count === 350) break
  	}
  	
  	return list
  }
  queryById(id: number): ListItem {
    let item: any;
    for (let i = 0; i < dataArr.length; i ++) {
      if (dataArr[i].id === id) {
        item = dataArr[i]
        break
      }
    }
    return item
  }
}
